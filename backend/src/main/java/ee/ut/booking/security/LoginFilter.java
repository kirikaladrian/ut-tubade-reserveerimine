package ee.ut.booking.security;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(-200)
public class LoginFilter implements Filter {

    private final String LOGOUT_URL = "/user/logout";
    private final String LOGIN_URL = "/login";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String requestURI = httpServletRequest.getRequestURI();

        if (requestURI.equals(LOGOUT_URL)) {
            Cookie[] cookies = httpServletRequest.getCookies();
            if(cookies != null){
                for(Cookie cookie : cookies){
                    cookie.setValue("");
                    cookie.setPath("/");
                    cookie.setMaxAge(0);
                    ((HttpServletResponse) response).addCookie(cookie);
                }
            }

            return;
        } else if (requestURI.equals(LOGIN_URL)) {
            httpServletRequest.getRequestDispatcher("/").forward(request, response);
            return;
        }

        chain.doFilter(request, response);
    }
}
