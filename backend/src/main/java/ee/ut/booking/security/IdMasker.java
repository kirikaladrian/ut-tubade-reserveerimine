package ee.ut.booking.security;

import at.favre.lib.idmask.Config;
import at.favre.lib.idmask.IdMask;
import at.favre.lib.idmask.IdMaskSecurityException;
import at.favre.lib.idmask.IdMasks;
import ee.ut.booking.db.entities.MaskedEntity;
import ee.ut.booking.db.entities.Room;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
@Aspect
@ControllerAdvice
public class IdMasker implements HandlerInterceptor {
    private final IdMask<Long> mask;

    public IdMasker(@Value("${application.security.mask.key}") byte[] key) {
        mask = IdMasks.forLongIds(Config.builder(key).build());
    }

    public String mask(Long l) {
        return mask.mask(l);
    }

    public String mask(Integer i) {
        if (i == null) {
            return null;
        }

        return mask.mask(i.longValue());
    }

    public String unmask(String masked) {
        return mask.unmask(masked).toString();
    }

    public Integer unmaskInt(String masked) {
        return mask.unmask(masked).intValue();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            Map<String, String> pathVariables = (Map<String, String>) request
                    .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

            for (String key : pathVariables.keySet()) {
                if (key.contains("id") || key.contains("Id")) {
                    pathVariables.put(key, unmask(pathVariables.get(key)));
                }
            }

        }

        return true;
    }

    @AfterReturning(pointcut = "execution(* ee.ut.booking.web.restlets.*.*(..)))", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        if (result instanceof MaskedEntity) {
            ((MaskedEntity) result).setMasker(this);
        }

        if (result instanceof ResponseEntity) {
            Object body = ((ResponseEntity<?>) result).getBody();

            if (body instanceof MaskedEntity) {
                ((MaskedEntity) body).setMasker(this);
            }

            if (body instanceof Iterable) {
                for (Object o : ((Iterable<?>) body)) {
                    if (o instanceof MaskedEntity) {
                        ((MaskedEntity) o).setMasker(this);
                    }
                }
            }

            if (body instanceof Map) {
                for (Object key : ((Map<?, ?>) body).keySet()) {
                    Object o = ((Map<?, ?>) body).get(key);
                    if (o instanceof MaskedEntity) {
                        ((MaskedEntity) o).setMasker(this);
                    }
                }
            }
        }
    }

    @ExceptionHandler(value = {IdMaskSecurityException.class, IllegalArgumentException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return ResponseEntity.notFound().build();
    }
}
