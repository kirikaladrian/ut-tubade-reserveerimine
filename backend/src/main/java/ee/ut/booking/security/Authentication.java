package ee.ut.booking.security;

import ee.ut.booking.db.entities.User;
import ee.ut.booking.db.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
public class Authentication {

    @Autowired
    private UserRepository userRepository;

    public User user() {
        User user = (User) request().getSession().getAttribute("user");

        if (user == null || User.ANON.equals(user.getName())) {
            return User.anonymous();
        }

        return userRepository.findById(user.getId()).orElseThrow();
    }

    private HttpServletRequest request() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

}
