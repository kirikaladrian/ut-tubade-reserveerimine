package ee.ut.booking.security;

import ee.ut.booking.db.entities.User;
import ee.ut.booking.db.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@Component
@Order(1)
public class UserFilter implements Filter {

    private final String ADMIN = "ROLE_ADMIN";

    @Autowired
    private UserRepository userRepository;

    @Override
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication instanceof AnonymousAuthenticationToken || req.getRequestURI().contains("/api/images")) {
            chain.doFilter(request, response);
            return;
        }

        OAuth2AuthenticationToken authenticationToken = (OAuth2AuthenticationToken) authentication;

        User user = null;
        if ("github".equals(authenticationToken.getAuthorizedClientRegistrationId())) {
            DefaultOAuth2User principal = ((DefaultOAuth2User) authentication.getPrincipal());

            Map<String, Object> attributes = principal.getAttributes();
            String name = ((String) attributes.get("login"));

            user = userRepository.findDistinctByUsername(name);

            if (user == null) {
                user = new User();

                user.setAdmin(false);
                user.setEmail(Optional.ofNullable(((String) attributes.get("email"))).orElse(""));
                user.setUsername(name);
                user.setName(Optional.ofNullable(((String) attributes.get("name"))).orElse(""));

                userRepository.save(user);
            }

            HttpSession session = req.getSession();
            session.setAttribute("user", user);
        } else if ("azure".equals(authenticationToken.getAuthorizedClientRegistrationId())) {
            DefaultOidcUser oidcUser = ((DefaultOidcUser) authentication.getPrincipal());

            Map<String, Object> attributes = oidcUser.getAttributes();
            String name = ((String) attributes.get("unique_name"));

            user = userRepository.findDistinctByUsername(name);

            if (user == null) {
                user = new User();

                user.setAdmin(false);
                user.setEmail(Optional.ofNullable(((String) attributes.get("upn"))).orElse(""));
                user.setUsername(name);
                user.setName(Optional.ofNullable(((String) attributes.get("name"))).orElse(""));

                userRepository.save(user);
            }

            HttpSession session = req.getSession();
            session.setAttribute("user", user);
        }

        if (user != null && user.isAdmin() && authenticationToken.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals(ADMIN))) {
            addAuthority(authenticationToken, new SimpleGrantedAuthority(ADMIN));
        }

        chain.doFilter(request, response);
    }

    private void addAuthority(OAuth2AuthenticationToken token, GrantedAuthority authority) {

        Collection<GrantedAuthority> authorities = new ArrayList<>(token.getAuthorities());
        authorities.add(authority);

        try {
            Field f1 = token.getClass().getSuperclass().getDeclaredField("authorities");
            setFinal(f1, token, authorities);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void setFinal(Field field, Object obj, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(obj, newValue);
    }
}