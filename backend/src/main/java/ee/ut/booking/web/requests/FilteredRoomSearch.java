package ee.ut.booking.web.requests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.ut.booking.db.entities.RoomType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
public class FilteredRoomSearch {
    private Integer capFrom;
    private Integer capTo;

    private Collection<String> equipment;
    private Collection<Integer> types;

    private String name;

    public Collection<RoomType> getTypes() {
        if (types == null) {
            return null;
        }

        return types.stream().map(RoomType::from).collect(Collectors.toList());
    }

    public static FilteredRoomSearch from(String searchString) throws JsonProcessingException {
        return new ObjectMapper().readValue(searchString, FilteredRoomSearch.class);
    }
}
