package ee.ut.booking.web.restlets;

import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.entities.Room;
import ee.ut.booking.db.repositories.ReservationRepository;
import ee.ut.booking.db.repositories.RoomRepository;
import ee.ut.booking.security.Authentication;
import ee.ut.booking.utils.Logger;
import ee.ut.booking.utils.Thumbnails;
import ee.ut.booking.web.requests.Booking;
import ee.ut.booking.web.requests.Request;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Stream;

@RestController
@Transactional
public class Rooms {

    private final RoomRepository roomRepository;

    private final Authentication authentication;

    private final ReservationRepository reservationRepository;

    private final Logger logger;

    public Rooms(RoomRepository roomRepository, Authentication authentication, ReservationRepository reservationRepository, Logger logger) {
        this.roomRepository = roomRepository;
        this.authentication = authentication;
        this.reservationRepository = reservationRepository;
        this.logger = logger;
    }

    @GetMapping("/api/rooms")
    public ResponseEntity<Iterable<Room>> rooms(@RequestParam(required = false, name = "search") String search) {
        Collection<Room> allWithFilter = Room.getAllWithFilter(roomRepository, search);
        return ResponseEntity.ok(allWithFilter);
    }

    @GetMapping("/api/rooms/{id}")
    public ResponseEntity<Room> room(@PathVariable Integer id) {
        Room room = roomRepository.findById(id).orElse(null);

        if (room == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(room);
    }

    @GetMapping("/api/rooms/{id}/availability/{date}")
    public ResponseEntity<Request.Response> roomAvailability(@PathVariable Integer id, @PathVariable String date) throws ParseException {

        Room room = roomRepository.findById(id).orElse(null);

        if (room == null) {
            return ResponseEntity.notFound().build();
        }

        Map<String, Stream<String>> response = room.availableAndBookedTimes(date);

        return ResponseEntity.ok(new Request.Response("ok").withPayload(response));
    }

    @PostMapping("/api/rooms/{id}/book")
    public Booking.Response book(@PathVariable Integer id, @RequestBody Booking bookRequest) throws ParseException {
        Room room = roomRepository.findById(id).orElse(null);

        Reservation reservation = new Reservation(authentication, bookRequest, room);
        reservationRepository.save(reservation);

        logger.log(String.format("User %s reserved room %s with params %s", authentication.user().getUsername(), id, bookRequest), Logger.Type.RESERVATION);

        return new Booking.Response("ok");
    }


    @GetMapping(value = "/api/images/{roomId}/a.jpg")
    @Cacheable("images")
    public ResponseEntity<byte[]> roomImage(@PathVariable Integer roomId) {
        Room room = roomRepository.findById(roomId).orElse(null);

        if (room == null) {
            return ResponseEntity.notFound().build();
        }

        byte[] image = room.getImage().getData();

        return ResponseEntity.ok(image);
    }
}
