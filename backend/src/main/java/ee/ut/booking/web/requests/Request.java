package ee.ut.booking.web.requests;

import lombok.Getter;

public abstract class Request {

    @Getter
    public static class Response {
        String status;

        Object payload;

        public Response(String status) {
            this.status = status;
        }

        public Response withPayload(Object payload) {
            this.payload = payload;
            return this;
        }
    }
}
