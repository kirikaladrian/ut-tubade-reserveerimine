package ee.ut.booking.web.restlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.ut.booking.db.entities.Image;
import ee.ut.booking.db.entities.Room;
import ee.ut.booking.db.repositories.ImageRepository;
import ee.ut.booking.db.repositories.RoomRepository;
import ee.ut.booking.security.Authentication;
import ee.ut.booking.security.IdMasker;
import ee.ut.booking.utils.Logger;
import ee.ut.booking.web.requests.NewRoom;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Transactional
public class RoomAdmin {
    private final RoomRepository roomRepository;
    private final Logger logger;
    private final Authentication authentication;

    public RoomAdmin(RoomRepository roomRepository, Logger logger, Authentication authentication, IdMasker idMasker, ImageRepository imageRepository, CacheManager cacheManager) {
        this.roomRepository = roomRepository;
        this.logger = logger;
        this.authentication = authentication;
        this.idMasker = idMasker;
        this.imageRepository = imageRepository;
        this.cacheManager = cacheManager;
    }

    private final ImageRepository imageRepository;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/api/rooms/new", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Room> newRoom(@RequestParam("image") MultipartFile formImage, @RequestParam("data") String data) throws JsonProcessingException {

        NewRoom request = new ObjectMapper().readValue(data, NewRoom.class);

        Image image = new Image(formImage);
        imageRepository.save(image);

        Room room = new Room(request, image);
        roomRepository.save(room);

        System.out.println(room.getId());

        logger.log(String.format("Admin %s created a new room %s", authentication.user().getUsername(), data), Logger.Type.NEWROOM);

        return ResponseEntity.ok(room);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/api/rooms/{id}")
    public void deleteRoom(@PathVariable Integer id) {
        Room room = roomRepository.findById(id).orElse(null);

        if (room == null) {
            return;
        }

        if (room.getReservations().size() > 0){
            room.setHidden(true);
        } else {
            roomRepository.delete(room);
        }

        logger.log(String.format("Admin %s deleted room %s", authentication.user().getName(), room.getId()), Logger.Type.DELETEROOM);

    }

    private final IdMasker idMasker;
    private final CacheManager cacheManager;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/api/rooms/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> updateRoom(@RequestParam(value = "image", required = false) MultipartFile formImage, @RequestParam("data") String data) throws JsonProcessingException {
        NewRoom request = new ObjectMapper().readValue(data, NewRoom.class);

        Room room = roomRepository.findById(idMasker.unmaskInt(request.getMaskedId())).orElse(null);

        if (room == null) {
            return ResponseEntity.notFound().build();
        }

        cacheManager.getCache("images").evict(room.getId());

        Image image = null;

        if (formImage != null) {
            if (room.getImage() != null) {
                imageRepository.delete(room.getImage());
            }

            image = new Image(formImage);
            imageRepository.save(image);
        }

        Room updatedRoom = new Room(request, image);

        room.update(updatedRoom);

        logger.log(String.format("Admin %s updated room %s with params %s", authentication.user().getName(), room.getId(), data), Logger.Type.UPDATEROOM);

        return ResponseEntity.ok().build();
    }
}
