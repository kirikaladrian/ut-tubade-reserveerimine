package ee.ut.booking.web.restlets;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Logout {

    @GetMapping("/user/logout")
    public void logout() {}

}
