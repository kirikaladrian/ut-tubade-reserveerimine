package ee.ut.booking.web.restlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.ut.booking.db.entities.CheckIn;
import ee.ut.booking.db.entities.CheckOut;
import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.repositories.CheckInRepository;
import ee.ut.booking.db.repositories.CheckOutRepository;
import ee.ut.booking.db.repositories.ReservationRepository;
import ee.ut.booking.security.Authentication;
import ee.ut.booking.security.IdMasker;
import ee.ut.booking.utils.Logger;
import ee.ut.booking.utils.MailSender;
import ee.ut.booking.web.requests.Request;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.Map;

@RestController
@Transactional
public class Reservations {

    private final MailSender mailSender;

    private final ReservationRepository reservationRepository;

    private final Authentication authentication;

    private final CheckInRepository checkInRepository;

    private final IdMasker idMasker;

    public Reservations(MailSender mailSender, ReservationRepository reservationRepository, Authentication authentication, CheckInRepository checkInRepository, IdMasker idMasker, CheckOutRepository checkOutRepository, Logger logger) {
        this.mailSender = mailSender;
        this.reservationRepository = reservationRepository;
        this.authentication = authentication;
        this.checkInRepository = checkInRepository;
        this.idMasker = idMasker;
        this.checkOutRepository = checkOutRepository;
        this.logger = logger;
    }

    @GetMapping("/api/reservations/{id}")
    public ResponseEntity<Map<String, Object>> getReservation(@PathVariable Integer id) {
        Reservation reservation = reservationRepository.findById(id).orElse(null);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        CheckIn checkIn = checkInRepository.findDistinctByReservationAndUserEquals(reservation, authentication.user());
        CheckOut checkOut = checkOutRepository.findDistinctByReservationAndUserEquals(reservation, authentication.user());

        Map<String, Object> response = Map.of(
                "reservation", reservation,
                "checkedIn", checkIn != null,
                "checkedOut", checkOut != null,
                "isOwner", reservation.getUser().getId() == authentication.user().getId()
        );

        return ResponseEntity.ok(response);
    }

    @PostMapping("/api/reservations/share")
    public ResponseEntity<String> shareReservation(@RequestBody String body) {
        Map data;

        try {
            data = new ObjectMapper().readValue(body, Map.class);
        } catch (JsonProcessingException e) {
            return ResponseEntity.badRequest().build();
        }
        Reservation reservation = reservationRepository.findById(idMasker.unmaskInt((String) data.get("id"))).orElse(null);
        if (reservation == null) {
            return ResponseEntity.badRequest().build();
        }

        try {
            mailSender.sendEmail(((String) data.get("email")), reservation);

            logger.log(String.format("User %s shared reservation %s with %s", authentication.user().getName(), reservation.getId(), data.get("email")), Logger.Type.SHARE);

            return ResponseEntity.ok("ok");
        } catch (MessagingException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    private final Logger logger;

    @DeleteMapping(value = "/api/reservations/{id}")
    public void deleteReservation(@PathVariable Integer id) {
        Reservation reservation = reservationRepository.findById(id).orElse(null);

        if (reservation == null) {
            return;
        }

        if (!(reservation.getUser().getId() == authentication.user().getId())) {
            return;
        }

        logger.log(String.format("User %s deleted reservation id %s", authentication.user().getName(), id), Logger.Type.CANCELRESERVATION);

        reservationRepository.delete(reservation);
    }

    @PostMapping(value = "/api/reservations/{id}/checkin")
    public ResponseEntity<Request.Response> checkin(@PathVariable Integer id) {
        Reservation reservation = reservationRepository.findById(id).orElse(null);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        if (checkInRepository.findDistinctByReservationAndUserEquals(reservation, authentication.user()) != null) {
            return ResponseEntity.ok(new Request.Response("already checked in"));
        }


        CheckIn checkIn = new CheckIn(authentication.user(), reservation);
        checkInRepository.save(checkIn);

        logger.log(String.format("User %s checked in to room %s", authentication.user().getName(), reservation.getRoom().getId()), Logger.Type.CHECKIN);

        return ResponseEntity.ok(new Request.Response("ok"));
    }

    private final CheckOutRepository checkOutRepository;

    @PostMapping(value = "/api/reservations/{id}/checkout")
    public ResponseEntity<Request.Response> checkout(@PathVariable Integer id) {
        Reservation reservation = reservationRepository.findById(id).orElse(null);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        if (checkOutRepository.findDistinctByReservationAndUserEquals(reservation, authentication.user()) != null) {
            return ResponseEntity.ok(new Request.Response("already checked out"));
        }


        CheckOut checkOut = new CheckOut(authentication.user(), reservation);
        checkOutRepository.save(checkOut);

        logger.log(String.format("User %s checked out from room %s", authentication.user().getName(), reservation.getRoom().getId()), Logger.Type.CHECKOUT);

        return ResponseEntity.ok(new Request.Response("ok"));
    }
}
