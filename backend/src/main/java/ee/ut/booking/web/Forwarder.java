package ee.ut.booking.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Forwarder {
    @RequestMapping(value = {"/{path:[^\\.]*}", "/{path:^(?!api).*}/{path:[^\\.]*}"})
    public String forward(@PathVariable String path) {
        return "forward:/";
    }
}