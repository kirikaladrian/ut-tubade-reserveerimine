package ee.ut.booking.web.requests;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Booking extends Request {
        private String startTime;
        private String endTime;
        private String date;
}