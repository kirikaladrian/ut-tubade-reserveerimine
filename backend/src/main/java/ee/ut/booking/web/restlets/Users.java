package ee.ut.booking.web.restlets;

import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.entities.User;
import ee.ut.booking.db.repositories.UserRepository;
import ee.ut.booking.security.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController()
public class Users {

    @Autowired
    private Authentication authentication;

    @GetMapping("/api/user")
    public User user() {
        return authentication.user();
    }

    @GetMapping("/api/user/reservations")
    public ResponseEntity<Collection<Reservation>> userReservations() {
        return ResponseEntity.ok(authentication.user().getReservation());
    }

}
