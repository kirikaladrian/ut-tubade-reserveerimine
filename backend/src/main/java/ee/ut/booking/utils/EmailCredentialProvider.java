package ee.ut.booking.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Properties;

@Component
public class EmailCredentialProvider {


    public EmailCredentialProvider(
            @Value("${application.mail.host}") String mailHost,
            @Value("${application.mail.port}") String port,
            @Value("${application.mail.username}") String username,
            @Value("${application.mail.password}") String password,
            @Value("${application.mail.username}") String address
    ) {
        this.username = username;
        this.password = password;
        this.address = address;

        this.mailHost = mailHost;
        this.mailPort = port;
    }

    private final String mailPort;
    private final String mailHost;
    private final String username;
    private final String password;
    private final String address;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Properties getProperties() {
        Properties properties = new Properties();

        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", mailHost);
        properties.put("mail.smtp.port", mailPort);
        properties.put("mail.smtp.ssl.trust", mailHost);

        return properties;
    }

    public InternetAddress getInternetAddress() {
        try {
            return new InternetAddress(address);
        } catch (AddressException e) {
            throw new RuntimeException(e);
        }
    }
}
