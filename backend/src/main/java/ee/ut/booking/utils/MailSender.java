package ee.ut.booking.utils;

import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.entities.User;
import ee.ut.booking.security.IdMasker;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.util.RandomUidGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Date;
import java.text.SimpleDateFormat;

@Component
public class MailSender {
    @Autowired
    private EmailCredentialProvider credentialProvider;

    @Autowired
    private IdMasker idMasker;

    public void sendEmail(String email, Reservation reservation) throws MessagingException {
        Session session = Session.getInstance(credentialProvider.getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(credentialProvider.getUsername(), credentialProvider.getPassword());
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(credentialProvider.getInternetAddress());
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

        message.setSubject("You are invited to join a reserved room");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        java.util.Date startTime = Date.from(reservation.getStartTime().toInstant());
        java.util.Date endTime = Date.from(reservation.getEndTime().toInstant());
        String msg = String.format("Hey!<br><br>%s invited you to a room reservation.<br>" +
                        "If you join the reservation, please check in %s.<br><br>" +
                        "Details:<br>Date: %s<br>" +
                        "Time: %s to %s",
                reservation.getUser().getName(), getReservationLink(reservation),
                dateFormat.format(startTime), timeFormat.format(startTime),
                timeFormat.format(endTime));

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html; charset=UTF-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        byte[] calendar = createCalendarAttachment(reservation, startTime, endTime, reservation.getUser());

        MimeBodyPart attachmentPart = new MimeBodyPart();
        ByteArrayDataSource source = new ByteArrayDataSource(calendar, "text/calendar");
        attachmentPart.setDataHandler(new DataHandler(source));
        attachmentPart.setFileName("ical.ics");
        multipart.addBodyPart(attachmentPart);


        message.setContent(multipart);

        send(message);
    }

    private byte[] createCalendarAttachment(Reservation reservation, java.util.Date startTime, java.util.Date endTime, User organizer) {
        //        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
//        TimeZone timezone = registry.getTimeZone(timeZone);
//        VTimeZone tz = timezone.getVTimeZone();
        VEvent vEvent = new VEvent(new net.fortuna.ical4j.model.DateTime(startTime), new net.fortuna.ical4j.model.DateTime(endTime), reservation.getRoom().getName());
//        vEvent.getProperties().add(tz.getTimeZoneId());
        vEvent.getProperties().add(new RandomUidGenerator().generateUid());
        vEvent.getProperties().add(new Description(String.format("Reservation for room %s at Delta", reservation.getRoom().getName())));
        vEvent.getProperties().add(new Organizer());
        vEvent.getOrganizer().getParameters().add(new Cn(organizer.getName()));
        String email = organizer.getEmail();

        if (!email.equals("")) {
            try {
                vEvent.getOrganizer().setValue(String.format("mailto:%s", email));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(new ProdId("-//FredBet//iCal4j 1.0//EN"));
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(Version.VERSION_2_0);

        icsCalendar.getComponents().add(vEvent);


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        CalendarOutputter outputter = new CalendarOutputter();
        try {
            outputter.output(icsCalendar, stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return stream.toByteArray();
    }

    private void send(Message    message) throws MessagingException {
        Transport.send(message);
    }

    private String getReservationLink(Reservation reservation) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String url = String.format("https://%s/booking/%s", request.getServerName(), idMasker.mask(reservation.getId()));
        return String.format("<a href='%s'>here</a>", url);
    }

}