package ee.ut.booking.utils;

import ee.ut.booking.db.entities.Image;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SystemUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Thumbnails {

    private static final String THUMBNAIL_DIM = "64";

    public enum Dim {
        THUMBNAIL("64"), DEFAULT("600");
        private final String size;

        Dim(String size) {
            this.size = size;
        }
    }

    public static byte[] create(byte[] data, Dim dim) {
        Image image = new Image(data, "");
        return create(image, dim);
    }

    public static byte[] create(Image image, Dim dim) {
        if (image == null) {
            throw new RuntimeException("Image is null");
        }

        log.info(String.format("Converting image %s to size %sx%s", image.getName(), dim.size, dim.size));

        Path tempImg = null;
        byte[] bytes;
        try {
            tempImg = Files.createTempFile("img_", ".png");

            try (FileOutputStream fos = new FileOutputStream(tempImg.toFile())) {
                fos.write(image.getData());
            }

            ProcessBuilder processBuilder = convertProcess(tempImg, dim);
            Process process = processBuilder.start();
            process.waitFor();
            bytes = Files.readAllBytes(tempImg);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            if (tempImg != null) {
                try {
                    Files.delete(tempImg);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

        }

        return bytes;
    }

    private static ProcessBuilder convertProcess(Path tempImg, Dim dim) {
        String tempFilePath = tempImg.toAbsolutePath().toString();

        List<String> command = commandName();
        command.addAll(List.of(tempFilePath,
                "-colors",
                "255",
                "-resize",
                String.format("%sx%s", dim.size, dim.size),
                tempFilePath));

        return new ProcessBuilder(command);
    }

    private static List<String> commandName() {
        if (SystemUtils.IS_OS_LINUX) {
            return new ArrayList<>(List.of("convert"));
        }

        return new ArrayList<>(List.of("magick", "convert"));
    }

}
