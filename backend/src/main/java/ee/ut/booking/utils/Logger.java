package ee.ut.booking.utils;

import ee.ut.booking.db.entities.Log;
import ee.ut.booking.db.entities.LogType;
import ee.ut.booking.db.repositories.LogRepository;
import ee.ut.booking.db.repositories.LogTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Logger {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private LogTypeRepository logTypeRepository;

    public void log(String message, Type type) {
        log.info(message);

        logRepository.save(new Log(message, fromType(type)));
    }

    public enum Type {
        RESERVATION("RESERVATION"), NEWROOM("NEWROOM"), UPDATEROOM("UPDATEROOM")
        , CHECKIN("CHECKIN"), CHECKOUT("CHECKOUT"), CANCELRESERVATION("CANCELRESERVATION"),
        SHARE("SHARE"), DELETEROOM("DELETEROOM")
        ;

        private final String name;

        Type(String name) {
            this.name = name;
        }

    }

    private LogType fromType(Type type) {
        return logTypeRepository.findDistinctByName(type.name);
    }
}
