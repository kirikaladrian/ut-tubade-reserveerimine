package ee.ut.booking.db.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.ut.booking.security.Authentication;
import ee.ut.booking.security.IdMasker;
import ee.ut.booking.web.requests.Booking;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

@Entity
@Table(name = "reservation")
@Getter
@Setter
public class Reservation extends MaskedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservationId")
    @SequenceGenerator(name = "reservationId", sequenceName = "reservation_id_seq", allocationSize = 1)
    @JsonIgnore
    private int id;

    @Override
    Integer id() {
        return getId();
    }

    @Override
    public void setMasker(IdMasker masker) {
        super.setMasker(masker);
        getRoom().setMasker(masker);
    }

    @Column
    private Timestamp startTime;

    @Column
    private Timestamp endTime;

    @Column
    private boolean isCancelled;

    @ManyToOne
    private Room room;

    @ManyToOne
    @JsonIgnore
    private User user;

    public Reservation() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return id == that.id &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startTime, endTime);
    }

    @OneToMany(mappedBy = "reservation")
    @JsonIgnore
    private Collection<CheckIn> checkIn;

    @OneToMany(mappedBy = "reservation")
    @JsonIgnore
    private Collection<CheckOut> checkOut;

    public Reservation(Authentication authentication, Booking bookRequest, Room room) throws ParseException {
        setUser(authentication.user());
        setRoom(room);

        String date = bookRequest.getDate();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Tallinn"));

        setStartTime(Timestamp.from(dateFormat.parse(date + " " + bookRequest.getStartTime()).toInstant()));

        if (bookRequest.getEndTime() == null) {
            setEndTime(Timestamp.from(DateUtils.addHours(dateFormat.parse(date + " " + bookRequest.getStartTime()), 1).toInstant()));
        } else {
            setEndTime(Timestamp.from(dateFormat.parse(date + " " + bookRequest.getEndTime()).toInstant()));
        }
    }
}
