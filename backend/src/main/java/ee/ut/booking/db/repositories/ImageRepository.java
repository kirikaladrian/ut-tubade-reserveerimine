package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Integer> {
}
