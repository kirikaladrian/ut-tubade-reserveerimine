package ee.ut.booking.db.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "check_in")
@Getter
@Setter
@NoArgsConstructor
public class CheckIn {

    public CheckIn(User user, Reservation reservation) {
        this.user = user;
        this.reservation = reservation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "check_inId")
    @SequenceGenerator(name = "check_inId", sequenceName = "check_in_id_seq", allocationSize = 1)
    private int id;

    @Column
    @CreationTimestamp
    private Timestamp time;

    @ManyToOne
    private User user;

    @ManyToOne
    private Reservation reservation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckIn checkIn = (CheckIn) o;
        return id == checkIn.id &&
                Objects.equals(time, checkIn.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time);
    }
}
