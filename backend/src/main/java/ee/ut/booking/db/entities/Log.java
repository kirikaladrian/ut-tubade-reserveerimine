package ee.ut.booking.db.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "log")
@Getter
@Setter
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "logId")
    @SequenceGenerator(name = "logId", sequenceName = "log_id_seq", allocationSize = 1)
    private int id;

    @Column
    private String message;

    @Column
    @CreationTimestamp
    private Timestamp timestamp;

    @ManyToOne
    private LogType type;

    public Log(String message, LogType type) {
        this.message = message;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Log log = (Log) o;
        return id == log.id &&
                Objects.equals(message, log.message) &&
                Objects.equals(timestamp, log.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, message, timestamp);
    }
}
