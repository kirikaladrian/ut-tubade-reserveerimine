package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.CheckOut;
import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface CheckOutRepository extends CrudRepository<CheckOut, Integer> {
    CheckOut findDistinctByReservationAndUserEquals(Reservation reservation, User user);
}
