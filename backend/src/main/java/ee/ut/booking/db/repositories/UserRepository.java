package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findDistinctByUsername(String username);
}
