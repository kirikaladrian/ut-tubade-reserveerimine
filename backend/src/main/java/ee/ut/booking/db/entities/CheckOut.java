package ee.ut.booking.db.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "check_out")
@Getter
@Setter
@NoArgsConstructor
public class CheckOut {

    public CheckOut(User user, Reservation reservation) {
        this.user = user;
        this.reservation = reservation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "check_outId")
    @SequenceGenerator(name = "check_outId", sequenceName = "check_out_id_seq", allocationSize = 1)
    private int id;

    @Column
    @CreationTimestamp
    private Timestamp time;

    @ManyToOne
    private User user;

    @ManyToOne
    private Reservation reservation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckOut checkOut = (CheckOut) o;
        return id == checkOut.id &&
                Objects.equals(time, checkOut.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time);
    }
}
