package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.LogType;
import org.springframework.data.repository.CrudRepository;

public interface LogTypeRepository extends CrudRepository<LogType, Integer> {
    LogType findDistinctByName(String name);
}
