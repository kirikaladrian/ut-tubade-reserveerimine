package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.Room;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room, Integer> {

    @Cacheable(value = "myCache")
    Room findDistinctByName(String name);

    @Query(value = "select id, name, seats, equipment, description from room ", nativeQuery = true)
    Collection<Room> roomsWithoutImage();

    @Override
    @Query("from Room r where r.hidden = false")
    Collection<Room> findAll();

    @Override
    @Query("from Room r where r.hidden = false and r.id = :integer")
    Optional<Room> findById(Integer integer);
}
