package ee.ut.booking.db.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "public")
@Getter
@Setter
public class User extends MaskedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userId")
    @SequenceGenerator(name = "userId", sequenceName = "user_id_seq", allocationSize = 1)
    @JsonIgnore
    private int id;

    @Override
    Integer id() {
        return getId();
    }

    @Column
    private String email;

    @Column
    private String name;

    @Column
    private boolean isAdmin;

    @Column
    private String username;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Reservation> reservation;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<CheckIn> checkIn;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<CheckOut> checkOut;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(email, user.email) &&
                Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, name);
    }

    public static final String ANON = "__anonymous__";

    public static User anonymous() {
        User user = new User();
        user.setAdmin(false);
        user.setName(ANON);
        user.setId(-1);
        return user;
    }
}
