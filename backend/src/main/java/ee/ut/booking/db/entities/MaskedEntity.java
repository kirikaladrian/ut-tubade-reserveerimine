package ee.ut.booking.db.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ee.ut.booking.security.IdMasker;

import javax.persistence.Transient;

public abstract class MaskedEntity {
    abstract Integer id();

    @Transient
    @JsonProperty("id")
    private String maskedId;

    @Transient
    @JsonIgnore
    private IdMasker masker;

    public String getMaskedId() {
        return masker.mask(id());
    }

    public void setMasker(IdMasker masker) {
        this.masker = masker;
    }
}
