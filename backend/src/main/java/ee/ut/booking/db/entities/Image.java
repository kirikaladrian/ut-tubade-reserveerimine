package ee.ut.booking.db.entities;

import ee.ut.booking.utils.Thumbnails;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "imageId")
    @SequenceGenerator(name = "imageId", sequenceName = "image_id_seq", allocationSize = 1)
    private int id;

    @Column
    private byte[] data;

    @Column
    private String name;

    public Image(MultipartFile image) {
        try {
            data = Thumbnails.create(image.getBytes(), Thumbnails.Dim.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        name = image.getOriginalFilename();
    }

    public Image(byte[] data, String name) {
        this.data = data;
        this.name = name;
    }
}
