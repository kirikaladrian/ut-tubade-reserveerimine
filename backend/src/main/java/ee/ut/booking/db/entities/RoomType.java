package ee.ut.booking.db.entities;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum RoomType {
    STUDENT_ROOM(0), SKYPE_BOOTH(1);

    @JsonValue
    private final int value;

    RoomType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RoomType from(Integer value) {
        if (value == null) {
            return null;
        }
        return Arrays.stream(RoomType.values()).filter(roomType -> roomType.value == value).findFirst().orElse(null);
    }
}
