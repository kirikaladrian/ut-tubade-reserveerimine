package ee.ut.booking.db.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.ut.booking.db.repositories.RoomRepository;
import ee.ut.booking.utils.Thumbnails;
import ee.ut.booking.web.requests.FilteredRoomSearch;
import ee.ut.booking.web.requests.NewRoom;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Entity
@Table(name = "room")
@Getter
@Setter
@NoArgsConstructor
public class Room extends MaskedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roomId")
    @SequenceGenerator(name = "roomId", sequenceName = "room_id_seq", allocationSize = 1)
    @JsonIgnore
    private int id;

    @Override
    Integer id() {
        return getId();
    }

    @Column
    private String name;

    @Column
    private int seats;

    @Column
    private String equipment;

    @Column
    private RoomType type;

    @Column
    @JsonIgnore
    private byte[] thumbnail;

    @Transient
    @JsonProperty("thumbnail")
    private String thumbnailb64;

    public String getThumbnailb64() {
        if (thumbnail == null) {
            return null;
        }

        return Base64.getEncoder().encodeToString(thumbnail);
    }

    public void loadThumbnail() {
        if (thumbnail == null) {
            thumbnail = Thumbnails.create(image, Thumbnails.Dim.THUMBNAIL);
        }
    }

    @Column
    private String description;

    @Column
    private Boolean hidden = false;

    @OneToMany(mappedBy = "room")
    @JsonIgnore
    private Set<Reservation> reservations;

    @OneToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Image image;

    public Room(NewRoom request, Image image) {
        try {
            equipment = new ObjectMapper().writeValueAsString(request.getEquipment());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (image != null) {
            this.image = image;
            this.thumbnail = Thumbnails.create(image, Thumbnails.Dim.THUMBNAIL);
        }
        type = RoomType.from(request.getType());
        seats = request.getSeats();
        description = request.getDescription();
        name = request.getName();
    }

    public static Collection<Room> getAllWithFilter(RoomRepository repository, String search) {
        Collection<Room> all = repository.findAll();

        if (search == null) {
            return all;
        }

        FilteredRoomSearch roomSearch;
        try {
            roomSearch = FilteredRoomSearch.from(search);
        } catch (JsonProcessingException e) {
            return Collections.emptyList();
        }

        Stream<Room> stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        all.iterator(),
                        Spliterator.ORDERED
                ),
                false
        );

        if (roomSearch.getCapFrom() != null) {
            stream = stream.filter(room -> room.getSeats() >= roomSearch.getCapFrom());
        }


        if (roomSearch.getCapTo() != null) {
            stream = stream.filter(room -> room.getSeats() <= roomSearch.getCapTo());
        }


        if (roomSearch.getEquipment() != null && roomSearch.getEquipment().size() > 0) {
            stream = stream.filter(room -> roomSearch.getEquipment().stream().allMatch(s -> room.getEquipment().contains(s)));
        }

        if (roomSearch.getTypes() != null && roomSearch.getTypes().size() > 0) {
            stream = stream.filter(room -> roomSearch.getTypes().stream().anyMatch(roomType -> roomType.equals(room.getType())));
        }

        if (roomSearch.getName() != null) {
            stream = stream.filter(room -> room.name.contains(roomSearch.getName()));
        }

        return stream.collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id &&
                seats == room.seats &&
                Objects.equals(name, room.name) &&
                Objects.equals(equipment, room.equipment) &&
                Arrays.equals(thumbnail, room.thumbnail) &&
                Objects.equals(thumbnailb64, room.thumbnailb64) &&
                Objects.equals(description, room.description) &&
                Objects.equals(hidden, room.hidden) &&
                Objects.equals(reservations, room.reservations) &&
                Objects.equals(image, room.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name, seats, equipment, thumbnailb64, description, hidden, reservations, image);
        result = 31 * result + Arrays.hashCode(thumbnail);
        return result;
    }

    public Map<String, Stream<String>> availableAndBookedTimes(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date day = dateFormat.parse(date);

        List<Reservation> bookedOnSameDay = this.getReservations()
                .stream()
                .filter(reservation -> DateUtils.isSameDay(reservation.getStartTime(), day))
                .collect(Collectors.toList());

        Set<String> bookedTimes = new HashSet<>();

        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        hourFormat.setTimeZone(TimeZone.getTimeZone("Europe/Tallinn"));

        for (Reservation reservation : bookedOnSameDay) {
            for (int i = reservation.getStartTime().toLocalDateTime().getHour(); i < reservation.getEndTime().toLocalDateTime().getHour(); i++) {
                bookedTimes.add(hourFormat.format(DateUtils.setHours(day, i)));
            }
        }


        Set<String> availableTimes = new HashSet<>();

        for (int hour = 8; hour <= 20; hour++) {
            Date withHour = DateUtils.setHours(day, hour);
            availableTimes.add(hourFormat.format(withHour));
        }

        availableTimes.removeAll(bookedTimes);

        return Map.of(
                "available", availableTimes.stream().sorted(),
                "booked", bookedTimes.stream().sorted()
        );
    }

    public void update(Room updatedRoom) {
        this.name = updatedRoom.name;
        this.description = updatedRoom.description;
        this.equipment = updatedRoom.equipment;
        this.seats = updatedRoom.seats;
        this.type = updatedRoom.type;

        if (updatedRoom.image != null) {
            this.image = updatedRoom.image;
            this.thumbnail = updatedRoom.thumbnail;
        }
    }
}
