package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.Reservation;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<Reservation, Integer> {
}
