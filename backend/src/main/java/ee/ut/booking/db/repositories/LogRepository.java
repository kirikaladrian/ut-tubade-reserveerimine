package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.Log;
import org.springframework.data.repository.CrudRepository;

public interface LogRepository extends CrudRepository<Log, Integer> {
}
