package ee.ut.booking.db.repositories;

import ee.ut.booking.db.entities.CheckIn;
import ee.ut.booking.db.entities.Reservation;
import ee.ut.booking.db.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface CheckInRepository extends CrudRepository<CheckIn, Integer> {
    CheckIn findDistinctByReservationAndUserEquals(Reservation reservation, User user);
}
