package ee.ut.booking.db.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "log_type")
@Getter
@Setter
public class LogType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "log_typeId")
    @SequenceGenerator(name = "log_typeId", sequenceName = "log_type_id_seq", allocationSize = 1)
    private int id;

    @Column
    private String name;

    @Column
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogType logType = (LogType) o;
        return id == logType.id &&
                Objects.equals(name, logType.name) &&
                Objects.equals(description, logType.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
