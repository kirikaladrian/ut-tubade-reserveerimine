-- create role postgres;

create table "user"
(
    id       serial      not null
        constraint "PK_user"
            primary key,
    email    varchar(50) not null,
    name     varchar(50) not null,
    is_admin boolean     not null,
    username varchar
);

alter table "user"
    owner to postgres;

create table log_type
(
    id          serial      not null
        constraint "PK_logtype"
            primary key,
    name        varchar(50) not null,
    description varchar(250)
);

alter table log_type
    owner to postgres;

create table log
(
    id        serial        not null
        constraint "PK_log"
            primary key,
    message   varchar(2000) not null,
    timestamp timestamp     not null,
    type_id   integer       not null
        constraint "FK_74"
            references log_type
);

alter table log
    owner to postgres;

create index "fkIdx_74"
    on log (type_id);

create table image
(
    id   serial not null
        constraint image_pk
            primary key,
    data bytea  not null,
    name varchar
);

alter table image
    owner to postgres;

create table room
(
    id          serial        not null
        constraint "PK_room"
            primary key,
    name        varchar(50)   not null,
    seats       integer       not null,
    equipment   varchar(2000) not null,
    description varchar(2000),
    hidden      boolean default false,
    thumbnail   bytea,
    image_id    integer
        constraint room_image_id_fk
            references image,
    type        integer
);

alter table room
    owner to postgres;

create table reservation
(
    id           serial    not null
        constraint "PK_reservation"
            primary key,
    start_time   timestamp not null,
    end_time     timestamp not null,
    is_cancelled boolean   not null,
    user_id      integer   not null
        constraint "FK_15"
            references "user",
    room_id      integer   not null
        constraint "FK_58"
            references room
);

alter table reservation
    owner to postgres;

create index "fkIdx_15"
    on reservation (user_id);

create index "fkIdx_58"
    on reservation (room_id);

create table check_in
(
    id             serial    not null
        constraint "PK_check-in"
            primary key,
    time           timestamp not null,
    user_id        integer   not null
        constraint "FK_39"
            references "user",
    reservation_id integer   not null
        constraint "FK_45"
            references reservation
            on delete cascade
);

alter table check_in
    owner to postgres;

create index "fkIdx_39"
    on check_in (user_id);

create index "fkIdx_45"
    on check_in (reservation_id);

create table check_out
(
    id             serial    not null
        constraint "PK_check-out"
            primary key,
    time           timestamp not null,
    user_id        integer   not null
        constraint "FK_42"
            references "user",
    reservation_id integer   not null
        constraint "FK_48"
            references reservation
            on delete cascade
);

alter table check_out
    owner to postgres;

create index "fkIdx_42"
    on check_out (user_id);

create index "fkIdx_48"
    on check_out (reservation_id);