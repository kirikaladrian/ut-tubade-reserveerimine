# UT Tubade Reserveerimine
## Room reservation system for UT

Deployed application: [https://deltaqr.ut.ee/](https://deltaqr.ut.ee/)

This is a web application built for University of Tartu which allows students to easily reserve study rooms (on 2nd floor of Delta building) for studying or working.

The app will allow its users to see a list of all rooms, book any of them at a chosen date and time, and check in and out of the rooms. There will be an administrator view for adding new rooms and editing the information of existing ones.

There are also QR codes next to the study rooms in Delta, through which users can book the rooms directly.

See [**Wiki**](https://gitlab.com/kirikaladrian/ut-tubade-reserveerimine/-/wikis/home) for more info.
