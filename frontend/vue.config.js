module.exports = {
    devServer: {
        proxy: {
            "^/api/": {
                target: "http://localhost:80",
                ws: true,
                changeOrigin: true
            }
        }
    },

    outputDir: "../backend/src/main/resources/public",

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: true
      }
    }
}
