import moment from "moment";

export default {
    dateTime(x) {
        return moment(x).format('HH:mm MM/DD/YYYY');
    },
    date(x){
        return moment(x).format('MM/DD/YYYY');
    },
    time(x){
        return moment(x).format('HH:mm');
    }
};
