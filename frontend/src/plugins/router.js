import Login from "@/components/Login";
import Rooms from "@/components/Rooms";
import RoomBooking from "@/components/RoomBooking";
import Profile from "@/components/Profile";
import Management from "@/components/Management";
import Booking from "@/components/RoomBookingInfo";
import Scanner from "@/components/Scanner";
import NotFound from "@/components/NotFound";
import Logout from "@/components/Logout";
import Router from "vue-router";
import axios from "axios";
import store from '@/plugins/store';

const routes = [
    { path: '/login', component: Login },
    { path: '/', redirect: '/rooms' },
    { path: '/rooms', component: Rooms},
    { path: '/rooms/:id', component: RoomBooking},
    { path: '/profile', component: Profile},
    { path: '/management', component: Management},
    { path: '/booking/:id', component: Booking},
    { path: '/scanner', component: Scanner},
    { path: '/notfound', component: NotFound},
    { path: '/logout', component: Logout},
    { path: "*", component: NotFound }
]

const router = new Router({
    mode:'history',
    linkActiveClass: 'nav-link-active',
    linkExactActiveClass: 'nav-link-active',
    routes // short for `routes: routes`
})

const adminPaths = [
    "/management",

]

router.beforeEach(async (to, from, next) => {

    if (!store.state.user) {
        await store.dispatch("fetchUserData")
    }

    let adminPage = adminPaths.reduce((prev, curr) => {
        return prev || to.path.search(curr) >= 0;
    }, false);

    if (adminPage && !store.getters.isAdmin) {
        router.push("/rooms").catch(()=>{});
        return
    }

    next();
});

axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {
    if (error.response.status === 404) {
        router.push("/notfound");
    }
});

export default router;
