import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

import et from 'vuetify/es5/locale/et'
import en from 'vuetify/es5/locale/en'

Vue.use(Vuetify)

const opts = {
    theme: {
        themes: {
            light: {
                primary: '#C7B886',
                secondary: '#BFB3B3'
            }
        }
    },
    lang: {
        locales: { en, et },
        current: 'et',
    },
}

export default new Vuetify(opts);
