import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        user: undefined,
        filters: {}
    },

    getters: {
        isAnon: state => state.user && state.user.name === "__anonymous__",
        isAdmin: state => state.user && state.user.admin,
        filters: state => state.filters
    },

    mutations: {
        user (state, user) {
            state.user = user;
        },

        filters(state, filters) {
            state.filters = filters;
        }
    },

    actions: {
        async fetchUserData(context) {
            let response = await axios.get(process.env.VUE_APP_API_PATH + "/api/user", {withCredentials: true});
            context.commit("user", response.data);
        }
    }
})

export default store;