import {createLocalVue, mount} from '@vue/test-utils'
import RoomBooking from '@/components/RoomBooking.vue'
import Vuetify from 'vuetify'

describe('RoomBooking.vue', () => {
    const localVue = createLocalVue()
    let vuetify

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    it('book a room', async () => {
        //const router = new VueRouter( { path: '/rooms/:id', component: RoomBooking} )
        //await router.push('/rooms/18')

        // opens given room view
        const roomId = 23;
        const wrapper = mount(RoomBooking, {
            localVue,
            vuetify,
            mocks: {$route: {params: {id: roomId}}}
        });

        // sets booking data
        wrapper.setData({roomModalOpen: true, date: '2020-11-01', time: 0});

        await wrapper.vm.$nextTick()
        await wrapper.vm.$nextTick()
        wrapper.find('#roomBookingBtn').trigger('click');
        //console.log(wrapper.find('#roomBookingMsg').text())
    })
})
