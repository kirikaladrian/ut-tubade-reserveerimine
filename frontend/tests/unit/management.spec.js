import { createLocalVue, mount } from '@vue/test-utils'
import Management from "@/components/Management";
import Vuetify from 'vuetify'

describe('Management.vue', () => {
  const localVue = createLocalVue()
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('create new room', async () => {
    const wrapper = mount(Management, {
      localVue,
      vuetify
    })

    // opens tab
    wrapper.find('#createRoomTab').trigger('click')

    // waits to tab to open
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()

    // setting data
    const dummyFile = new File(["dummy"], "dummy.png", {type: "image/png"})
    wrapper.setData({roomName: 'UnitTestName', roomDesc: 'UnitTestDesc', roomSeats: 4, file: dummyFile, equipment: []})

    // room creation triggered
    wrapper.find('#createRoomBtn').trigger('click')

    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()

    // TODO
    // expect(wrapper.find('#createRoomMsg').text).toBe('New room successfully created!');
  })
})
